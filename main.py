import os, time
import requests, json
import configparser
from base64 import b64decode, b64encode

# SECRET:)
config_url = os.path.join(os.path.dirname(__file__), "config.ini")
config = configparser.ConfigParser()
config.read(config_url, 'UTF-8')

output_log = "True"
if config.has_option("DEFAULT", "OUTPUT_LOG"):
    output_log = config["DEFAULT"]["OUTPUT_LOG"]

def print_log(message):
    if output_log != "True":
        return
    print(message) 

def get_tweet_data(bearer_token, account, account_id):
    url = "https://api.twitter.com/2/users/" + account_id +"/tweets"
    
    since_id = 0
    if config.has_option(account, "newest_id") == True:
        since_id = config[account]["newest_id"]
    print_log("since_id:" + str(since_id))

    next_token = ""

    headers = {
        "Authorization": "Bearer {}".format(bearer_token)
    }

    tweets = []
    
    count = 0
    newest_id = 0

    page_cnt = 20
    if config.has_section(account) == False:
        page_cnt = 1

    print_log(page_cnt)

    while count < page_cnt:
        params = {
            "tweet.fields": "created_at,attachments,author_id,in_reply_to_user_id,referenced_tweets",
            "max_results": 20,
            # "until_id": "1469247802744393729"
            # "start_time": "2021-07-01T00:00:00Z",
            #"pagination_token": next_token,
            "since_id": since_id,
        }

        if(next_token != ""):
            params["pagination_token"] = next_token

        r = requests.get(url, headers=headers, params=params)
        if("errors" in  r.json()):
            print('an error occur by timeline request.')
            print(r.json())
            return tweets

        if("data" not in r.json()):
            print("no new tweet. account:" + account)
            return tweets

        tweets.extend(r.json()["data"])
        print_log("tweets. ")
        print_log(tweets)

        if(int(newest_id) < int(r.json()["meta"]["newest_id"])):
            newest_id = r.json()["meta"]["newest_id"]

        count = count + 1

        if "next_token" not in r.json()["meta"]:
            break
        else:
            next_token = r.json()["meta"]["next_token"]

    
    if(int(newest_id) > int(since_id)):
        if config.has_section(account) == False:
            config.add_section(account)
            config.set(account, "newest_id", newest_id)
            with open(config_url, 'w', encoding='UTF-8') as file:
                config.write(file)
            return []# 初回はnewest_idを取得するだけで終わりにする。
        config.set(account, "newest_id", newest_id)

        with open(config_url, 'w', encoding='UTF-8') as file:
            config.write(file)

    return tweets

def get_tweet(bearer_token, account, webhook_url):
    # アカウント情報を取得する。
    print_log("account:" + account)
    url = "https://api.twitter.com/2/users/by/username/" + account
    headers = {
        "Authorization": "Bearer {}".format(bearer_token)
    }
    params = {"user.fields":"profile_image_url"}

    r = requests.get(url, headers=headers, params=params)
    if("errors" in  r.json()):
        print('an error occur by usename request.')
        print(r.json())


    tweets = r.json()

    print_log("username json.")
    print_log(r.json())
    
    account_info = r.json()

    # ===== 3. Userのtimelineを取得 =====
    # ref: http://benalexkeen.com/interacting-with-the-twitter-api-using-python/
    # ref: https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline.html

    # url = "https://api.twitter.com/1.1/statuses/user_timeline.json"

    tweets = get_tweet_data(bearer_token, account, account_info["data"]["id"])
    tweets = sorted(tweets, key=lambda x: x.get("id", -1))

 
    print_log('tweet json.')
    print_log(tweets)

    #post_discord(tweets["data"], account_info, webhook_url)
    post_discord(tweets, account_info, webhook_url)



def post_discord(tweets, account_info, webhook_url):
    
    headers      = {'Content-Type': 'application/json'}

    letters = [];
    # サボり、とりあえず１つ文字列抽出ができればよい
    if config.has_option("DEFAULT", "pickup_letter1") and config["DEFAULT"]["pickup_letter1"]:
        print_log('has letter.')

        letter1 = {
            "letter": config["DEFAULT"]["pickup_letter1"],
            "discord_url": config["DEFAULT"]["pickup_webhook1"]
        }
        letters.append(letter1)


    for tweet in tweets:
        post_discord_inner(tweet, account_info, webhook_url,  headers)

        for letter in letters:
            print_log("letter = " + letter["letter"])
            print_log("tweet_text = " + tweet["text"])
            if letter["letter"] in tweet["text"]:
                print_log("letter is True")
                post_discord_inner(tweet, account_info, letter["discord_url"],  headers)

def post_discord_inner(tweet, account_info, webhook_url, headers):
    
    main_content = {
        "content": tweet["text"],
        "username": account_info["data"]["name"],
        "avatar_url":  account_info["data"]["profile_image_url"]
    }

    response = requests.post(webhook_url, json.dumps(main_content), headers=headers)
    print_log("post tweet to webhook")
    print_log(tweet)
    time.sleep(0.2)


def main():
    # ===== 手順 (プログラム外の手順も) =====
    # 1. twitterに申請してconsumer_key, consumer_key_secretの取得
    # ==== ここからプログラム ====
    # 2. bearer tokenの取得 (consumer_key, consumer_key_secretを使用)
    # 3. Userのtimelineを取得 (bearer token, user_timeline api を使用)
    # =======================================

    # 1. consumer_key, consumer_key_secretの取得
    # Application Managementのページから発行できる

    # ===== 2. bearer tokenの取得 =====
    # ref: https://developer.twitter.com/en/docs/basics/authentication/api-reference/token
    headers = { "Content-Type" : "application/x-www-form-urlencoded;charset=UTF-8" }
    data = { "grant_type":"client_credentials" }
    oauth2_url = "https://api.twitter.com/oauth2/token"
 
    r = requests.post(oauth2_url, data=data, headers=headers, auth=(config["DEFAULT"]["CONSUMER_KEY"], config["DEFAULT"]["CONSUMER_SECRET"]))
    if("errors" in  r.json()):
        print('consumer key:' + config["DEFAULT"]["CONSUMER_KEY"])
        print('consumer secret:' + config["DEFAULT"]["CONSUMER_SECRET"])
        print('an error occur by auth request.')
        print(r.json())
        return;

    bearer_token = r.json()["access_token"]
    print_log("[*] Bearer token:" + bearer_token + "\n")

    if config.has_option("DEFAULT", "TWITTER_ACCOUNT1"):
        webhook_url1 = config["DEFAULT"]["discord_webhook1"]
        twitter_account1 = config["DEFAULT"]["TWITTER_ACCOUNT1"]
        get_tweet(bearer_token, twitter_account1, webhook_url1)

    if config.has_option("DEFAULT", "TWITTER_ACCOUNT2"):
        webhook_url2 = config["DEFAULT"]["discord_webhook2"]
        twitter_account2 = config["DEFAULT"]["TWITTER_ACCOUNT2"]
        get_tweet(bearer_token, twitter_account2, webhook_url2)

    if config.has_option("DEFAULT", "TWITTER_ACCOUNT3"):
        webhook_url3 = config["DEFAULT"]["discord_webhook3"]
        twitter_account3 = config["DEFAULT"]["TWITTER_ACCOUNT3"]
        get_tweet(bearer_token, twitter_account3, webhook_url3)

    if config.has_option("DEFAULT", "TWITTER_ACCOUNT4"):
        webhook_url4 = config["DEFAULT"]["discord_webhook4"]
        twitter_account4 = config["DEFAULT"]["TWITTER_ACCOUNT4"]
        get_tweet(bearer_token, twitter_account4, webhook_url4)

    if config.has_option("DEFAULT", "TWITTER_ACCOUNT5"):
        webhook_url5 = config["DEFAULT"]["discord_webhook5"]
        twitter_account5 = config["DEFAULT"]["TWITTER_ACCOUNT5"]
        get_tweet(bearer_token, twitter_account5, webhook_url5)



if __name__ == "__main__":
    main()




